/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author diegohenicka
 */
public class Dados {
    public static List<TipoObjeto> listaTipoObjeto = new ArrayList<>();
    public static List<Objeto> listaObjeto = new ArrayList<>();
    
    public static List<Pessoa> listaPessoa = new ArrayList<>();
    
    public static List<Emprestimo> listaEmprestimo = new ArrayList<>();
    public static List<EmprestimoItens> listaEmprestimoItens = new ArrayList<>();
    public static List<Manutencao> listaManutencao = new ArrayList<>();
    
}
