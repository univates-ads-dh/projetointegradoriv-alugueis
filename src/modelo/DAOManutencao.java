/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.List;

/**
 *
 * @author diegohenicka
 */
public class DAOManutencao {
    public List<Manutencao> getLista(){
        return Dados.listaManutencao;
    }
    
    public boolean salvar(Manutencao obj){
        if (obj.getId() == null){
            Integer id = Dados.listaManutencao.size()+1;
            obj.setId(id);
            Dados.listaManutencao.add(obj);
        }
        return true;
    }
    
    public boolean remover(Manutencao obj){
        Dados.listaManutencao.remove(obj);
        return true;
    }
    
}
