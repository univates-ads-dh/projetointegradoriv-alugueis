/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author diegohenicka
 */
public class EmprestimoItens {
    private Integer id;
    private Integer id_emprestimo;
    private Integer id_objeto;
    private String status;    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_emprestimo() {
        return id_emprestimo;
    }

    public void setId_emprestimo(Integer id_emprestimo) {
        this.id_emprestimo = id_emprestimo;
    }

    public Integer getId_objeto() {
        return id_objeto;
    }

    public void setId_objeto(Integer id_objeto) {
        this.id_objeto = id_objeto;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    
}
