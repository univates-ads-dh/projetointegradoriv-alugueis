/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.List;

/**
 *
 * @author diegohenicka
 */
public class DAOEmprestimoItens {
    public List<EmprestimoItens> getLista(){
        return Dados.listaEmprestimoItens;
    }
    
    public boolean salvar(EmprestimoItens obj){
        if (obj.getId() == null){
            Integer id = Dados.listaEmprestimoItens.size()+1;
            obj.setId(id);
            Dados.listaEmprestimoItens.add(obj);
        }
        return true;
    }
    
    public boolean remover(EmprestimoItens obj){
        Dados.listaEmprestimoItens.remove(obj);
        return true;
    }
    
}
