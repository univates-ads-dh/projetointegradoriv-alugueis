/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.List;

/**
 *
 * @author diegohenicka
 */
public class DAOPessoa {
    public List<Pessoa> getLista(){
        return Dados.listaPessoa;
    }
    
    public boolean salvar(Pessoa obj){
        if (obj.getId() == null){
            Integer id = Dados.listaPessoa.size()+1;
            obj.setId(id);
            Dados.listaPessoa.add(obj);
        }
        return true;
    }
    
    public boolean remover(Pessoa obj){
        Dados.listaPessoa.remove(obj);
        return true;
    }
    
}
