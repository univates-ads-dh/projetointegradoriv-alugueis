/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.List;

/**
 *
 * @author diegohenicka
 */
public class DAOTipoObjeto {
    
    public List<TipoObjeto> getLista(){
        return Dados.listaTipoObjeto;
    }
    
    public boolean salvar(TipoObjeto obj){
        if (obj.getId() == null){
            Integer id = Dados.listaTipoObjeto.size()+1;
            obj.setId(id);
            Dados.listaTipoObjeto.add(obj);
        }
        return true;
    }
    
    public boolean remover(TipoObjeto obj){
        Dados.listaTipoObjeto.remove(obj);
        return true;
    }
}
