/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.List;

/**
 *
 * @author diegohenicka
 */
public class DAOEmprestimo {
    public List<Emprestimo> getLista(){
        return Dados.listaEmprestimo;
    }
    
    public boolean salvar(Emprestimo obj){
        if (obj.getId() == null){
            Integer id = Dados.listaEmprestimo.size()+1;
            obj.setId(id);
            Dados.listaEmprestimo.add(obj);
        }
        return true;
    }
    
    public boolean remover(Emprestimo obj){
        Dados.listaEmprestimo.remove(obj);
        return true;
    }
    
}
