/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.List;

/**
 *
 * @author diegohenicka
 */
public class DAOObjeto {
    public List<Objeto> getLista(){
        return Dados.listaObjeto;
    }
    
    public boolean salvar(Objeto obj){
        if (obj.getId() == null){
            Integer id = Dados.listaObjeto.size()+1;
            obj.setId(id);
            Dados.listaObjeto.add(obj);
        }
        return true;
    }
    
    public boolean remover(Objeto obj){
        Dados.listaObjeto.remove(obj);
        return true;
    }
    
}
